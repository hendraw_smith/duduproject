package com.project.dudu.controller;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.project.dudu.model.User;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;

import java.util.*;

@Controller
public class UserApiController {


    @Value("${api.host.url}")
    public String API_HOST_URL;

    RestTemplate restTemplate = new RestTemplate();

    private User[] user;

    @GetMapping("/user/list")
    public String getUserList(Model model) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

        HttpEntity<String> entity = new HttpEntity<>("parameters", headers);
        ResponseEntity<String> response = restTemplate.exchange(  API_HOST_URL + "/user/list", HttpMethod.GET, entity, String.class);

        HttpStatus statusCode = response.getStatusCode();
        if (statusCode == HttpStatus.OK) {
            String data = response.getBody();
            ObjectMapper mapper = new ObjectMapper();
            try {
                List<User> userList = Arrays.asList(mapper.readValue(data, User[].class));
                model.addAttribute("userList", userList);
                return "list";
            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }
        }
        return null;
    }

    @RequestMapping(path = {"/user/list", "/user/list/byId"})
    private void getUserById() {
        Map<String, String> params = new HashMap<String, String>();
        params.put("id", "12");
        User user = restTemplate.getForObject(API_HOST_URL + "/user/list/", User.class, params);
        System.out.println(user);
    }

    @RequestMapping(value="/auth/login", method = RequestMethod.POST)
    public String createAuthLogin(String email, String password, Model model){
        if(email.contains("@")) {
            Map<String, String> map = new HashMap<>();
            map.put("email", email);
            map.put("password", password);

            ResponseEntity<String> response = restTemplate.postForEntity(API_HOST_URL + "/user/auth/login", map, String.class);
            if (response.getStatusCode() == HttpStatus.OK) {
                System.out.println("Request Successful");
                String data = response.getBody();
                ObjectMapper mapper = new ObjectMapper();
                try {
                    User[] user = mapper.readValue(data, User[].class);
                    if(user.length > 0) {
                        model.addAttribute("email", Arrays.stream(user).findAny().get().getEmail());
                        model.addAttribute("name", Arrays.stream(user).findAny().get().getFull_name());
                        return "redirect:/create-registry";

                    }else{
                        return "index";
                    }
                } catch (JsonProcessingException e) {
                    throw new RuntimeException(e);
                }
            } else {
                System.out.println("Request Failed");
            }
        }else {
            return "index";
        }
        return "index";
    }
}
