package com.project.dudu.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class NotificationCtr {

	@GetMapping("/notification")
	public String notification(){
		return "notifications/notification";
	}
}
