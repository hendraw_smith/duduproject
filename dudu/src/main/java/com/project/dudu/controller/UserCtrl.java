package com.project.dudu.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class UserCtrl {

	@GetMapping("/user-profile")
	public String profile(ModelMap modelMap){
		modelMap.addAttribute("user-profile", "active");
		return "user/user-profile";
	}

	@GetMapping("/user-profile/user-favorite")
	public String favorite(ModelMap modelMap) {
		modelMap.addAttribute("user-favorite", "active");
		return "user/user-favorite";
	}

	@GetMapping("/user-profile/user-activity")
	public String activity(ModelMap modelMap) {
		modelMap.addAttribute("user-favorite", "active");
		return "user/user-activity";
	}
}
