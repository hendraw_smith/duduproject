package com.project.dudu.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class CreaterRegistryCtr {

	@GetMapping("/create-registry")
	public String index() {

		return "createRegistry/index";
	}

	@GetMapping("/create-registry/register")
	public String register(){
		return "createRegistry/register-registry";
	}

	@GetMapping("/create-registry/user/list-registry")
	public String listRegistry(){
		return "createRegistry/list-registry";
	}

}
