package com.project.dudu.controller.Register;

import com.project.dudu.controller.email.EmailSenderService;
import com.project.dudu.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Controller
public class RegisterCtr {

    @Autowired
    EmailSenderService senderService;

    @Value("${api.host.url}")
    public String API_HOST_URL;

    RestTemplate restTemplate = new RestTemplate();

    static String getAlphaNumericString(int n)
    {
        String AlphaNumericString = "0123456789";
        StringBuilder sb = new StringBuilder(n);
        for (int i = 0; i < n; i++) {
            int index = (int)(AlphaNumericString.length() * Math.random());
            sb.append(AlphaNumericString.charAt(index));
        }
        return sb.toString();
    }

    @GetMapping(value = "/register/step1")
    public String registerStep1(){
        return "/user/register";
    }

    @RequestMapping(value = "/save/register1", method = RequestMethod.POST)
    public String saveRegister1(String email, String full_name, String code){
        sendMail(email,code);
        return "/index";
    }

    @RequestMapping(value = "/register/step2", method= RequestMethod.POST)
    public String registerStep2(Model model, String email_user, String fullName, String code, String len_1,
                                String len_2, String len_3, String len_4, String len_5, String len_6){
        String codestep2 = len_1+len_2+len_3+len_4+len_5+len_6;
        if(code.equals(codestep2)){
            model.addAttribute("email", email_user);
            model.addAttribute("name", fullName);
            model.addAttribute("code", code);
            return "/user/register2";
        }
        else
            return "/user/wrong-totp";

    }

    @RequestMapping(value = "/save/user", method = RequestMethod.POST)
    public String UserSave(String email_user, String fullName, String username, String password, String dob){
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        Map<String, Object> map = new HashMap<>();
        map.put("role_id", String.valueOf(1));
        map.put("full_name", fullName);
        map.put("dob", dob);
        map.put("phone", null);
        map.put("email", email_user);
        map.put("sex", null);
        map.put("username", username);
        map.put("password", password);
        HttpEntity<Map<String, Object>> entity = new HttpEntity<>(map, headers);

        ResponseEntity<String> response = restTemplate.postForEntity(API_HOST_URL+"/user/create/save", entity, String.class);
        if (response.getStatusCode() == HttpStatus.OK) {
            System.out.println("Request Successful");
            return "index";
        } else {
            System.out.println("Request Failed");
        }
        return null;
    }

    public void sendMail(String email, String code){
        senderService.sendEmail(email, "Dudu Info Register", code);
    }
}
