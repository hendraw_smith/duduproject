package com.project.dudu.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.ui.Model;

@Controller
public class IndexController {

    @GetMapping("/")
    public String hello(Model model){
        model.addAttribute("nama", "Hendra Wijaya");
        return "index";
    }


}
