// primary color

$primaryColor: #e9b44c;
$seconColor: #80a3fd;
$clr-color: #002fa7;
$clr-primary: #fbd075;
$clr-danger: #4d1300;
$clr-primary-light: #adffff;
$clr-primary-dark: #091034;
$clr-gray100: #f9fbff;
$clr-gray150: #f4f6fb;
$clr-gray200: #eef1f6;
$clr-gray300: #e1e5ee;
$clr-gray400: #767b91;
$clr-gray500: #4f546c;
$clr-gray600: #2a324b;
$clr-gray700: #161d34;
$clr-bg: #060b23;

body {
  background-repeat: no-repeat;
  background-size: cover;
}

.navbarNav {
  background-color: $primaryColor;
}

.search-form {
  position: absolute;
  border-color: black;
  background-color: transparent;
}

.search-button {
  color: black;
  background-color: transparent;
  border-color: black;
}

header.top .topmenu li a:hover {
  font-weight: bold;
  border-bottom: 2px solid $clr-color;
}

/* FontAwesome */

.fa {
  font-family: FontAwesome;
  font-style: normal;
  font-size: 20px;

  .fa-search {
    bottom: 4px;
    position: absolute;
    right: 90%;
    margin-bottom: 3px;
    padding-bottom: 3px;
  }
}

.search-bar {
  .fontuser {
    position: relative;
    padding: 12px 40px;
    margin: 8px 0;

    border: 1px solid #ccc;
    box-sizing: border-box;
  }

  i {
    position: absolute;
    display: inline-block;
    left: 15px;
    top: 40px;
    color: gray;
  }
}

.fa-search,
button {
  cursor: pointer;
}

.search-bar .fa-search,
.topmenu .search i {
  position: relative;
}

header.top .search-bar form button {
  width: 4rem;
  font-size: 2rem;
  background: none;
  border: 0;
}

header.top .search-bar form .search-input:-ms-input-placeholder {
  opacity: 1;
  color: #c8caca;
}

header.top .search-bar form .search-input,
header.top .search-bar form button {
  height: 60%;
}

header.top .topmenu li a {
  color: #000;
  display: block;
  text-decoration: none;
}

header.top .topmenu li.search a:hover {
  color: $seconColor;
  border-bottom: none;
}

header.top .search-bar {
  display: none;
  position: absolute;
  top: 0px;
  left: 0;
  width: 100%;
  height: 100%;
  background: $primaryColor;
  text-align: center;
}

header.top .search-bar form {
  margin-top: 0;
  width: 100%;
  height: 100%;
}

.top .search-bar form .search-input {
  width: 480px;
  font-size: 20px;
  color: #000;
  font-weight: 600;
  border-radius: 0;
  padding-bottom: 3px;
}

.search-input,
.search-input:focus {
  border: none;
  border-bottom: 1px solid #000;
  outline: none;
  -webkit-box-shadow: none;
  box-shadow: none;
  background-color: transparent;
  margin-bottom: 12px;
}

.social-medial {
  margin-bottom: 40px;
  position: relative;
  display: flex;

  img {
    margin-top: 10px;
    margin-right: 0.5rem;
    height: 2rem;
    transition: 0.5s;
  }

  img:hover {
    border-radius: 20px;
    -webkit-transform: scale(1.5);
    transform: scale(1.5);
    margin-right: 0.9rem;
  }
}

.card {
  background-color: $clr-primary;
  border-radius: 20px;
}

.form {
  position: relative;
  height: 3rem;

  &__input {
    position: absolute;
    top: 0;
    left: 0;
    border: 2px solid black;
    border-radius: 10px;
    font-family: inherit;
    font-size: 14px;
    font-weight: bold;
    color: black;
    outline: none;
    padding: 1.25rem;
    background: none;

    &:hover {
      // border-color: $clr-primary-light;

      background: transparent;
    }

    &:focus {
      // border-color: $clr-primary;
      background: transparent;
      border: 2px solid black;
      outline: none;
    }
  }

  &__label {
    position: absolute;
    left: 1rem;
    top: 0.5rem;
    // color: white;
    padding: 0 1rem;
    cursor: text;
    transition: top 200ms ease-in, left 200ms ease-in, font-size 200ms ease-in;
    // background-color: $clr-bg;
  }
}

.form__input:focus ~ .form__label,
.form__input:not(:placeholder-shown).form__input:not(:focus) ~ .form__label {
  top: -0.8rem;
  font-size: 0.9rem;
  left: 0.9rem;
  background: $clr-primary;
}

.modal-dialog .fm_costum {
  margin-right: 15px;
  border: 1px solid black;
  background: $clr-primary;
  height: 50px;
  padding: 0 20px;
  font-size: 20px;
  font-weight: bold;
}

.banner {
  background-image: url(../assets/image/bg.png);
  // position: relative;
  min-height: 100vh;
  padding: 150px 0 200px;
  background-repeat: no-repeat;
  background-size: cover;

  h4 {
    color: $clr-danger;
    font-family: "Segoe UI", Tahoma, Geneva, Verdana, sans-serif;
    font-size: 30px;
  }

  h1 {
    font-size: 40px;
    color: $seconColor;
    padding: 1px 0 5px;
    line-height: 1.6;
    font-family: "Segoe UI", Tahoma, Geneva, Verdana, sans-serif;
  }
}

#service {
  .service-h4 {
    padding-bottom: 30px;
  }

  a {
    text-decoration: none;
    color: black;

  }

  .service-icon {
    width: 60px;
    height: 60px;
    display: flex;
    align-items: center;
    justify-content: center;
    border-radius: 60px;
    box-shadow: 0px 1px 7px rgba(0, 0, 0, 0.5);
    background: $clr-gray300;
  }

  .service-box {
    height: 300px;
    transition: .3s transform cubic-bezier(.155, 1.105, .295, 1.12), .3s box-shadow, .3s -webkit-transform cubic-bezier(.155, 1.105, .295, 1.12);
    cursor: pointer;
  }

  .service-box:hover {
    transform: scale(1.05);
    box-shadow: 0 10px 20px rgba(0, 0, 0, .12), 0 4px 8px rgba(0, 0, 0, .06);
  }
}

.footers {
  background: $clr-primary;
  color: $clr-danger;

  a {
    text-decoration: none;
    color: $clr-danger;
    background-image: linear-gradient(
                    to right,
                    #54b3d6,
                    #54b3d6 50%,
                    #000 50%
    );
    background-size: 200% 100%;
    background-position: -100%;
    display: inline-block;
    padding: 5px 0;
    position: relative;
    -webkit-background-clip: text;
    -webkit-text-fill-color: transparent;
    transition: all 0.3s ease-in-out;
  }

  a:before {
    content: '';
    background: #347afc;
    display: block;
    position: absolute;
    bottom: -3px;
    left: 0;
    width: 0;
    height: 3px;
    transition: all 0.3s ease-in-out;
  }

  a:hover {
    background-position: 0;
  }

  a:hover::before {
    width: 100%;
  }
}

.jmb {
  position: relative;
  background-image: url(../assets/image/bg2.png);
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  margin-top: 30px;
  height: 100vh;

  .text-banner {
    position: absolute;
    font-family: "Segoe UI", Tahoma, Geneva, Verdana, sans-serif;
  }

  .jmb-about {
    vertical-align: middle;
    z-index: 1;

    .jmb-about-card {
      height: 300px;
      border-radius: 10px;
    }
  }
}

.card-img-top {
  height: 300px;
  background-size: cover;
}

.card-no-border .card {
  border-color: #d7dfe3;
  border-radius: 4px;
  margin-bottom: 30px;
  -webkit-box-shadow: 0px 5px 20px rgba(0, 0, 0, 0.05);
  box-shadow: 0px 5px 20px rgba(0, 0, 0, 0.05)
}

.pro-img {
  margin-top: -125px;
  margin-left: 20px;
}

.little-profile .pro-img img {
  width: 128px;
  height: 128px;
  border: 1px solid #c1c1c1;
  //-webkit-box-shadow: 0 0 15px rgba(0, 0, 0, 0.2);
  //box-shadow: 0 0 15px rgba(0, 0, 0, 0.5);
  //border-radius: 100%
}

.nav-profile {
  .profile-nav {
    position: relative;
    font-weight: bold;
    font-size: 15px;
    color: black;
    font-family: "Segoe UI", Tahoma, Geneva, Verdana, sans-serif;
    line-height: 32px;
    letter-spacing: 3px;
  }

  .profile-nav li a:hover {
    border-bottom: 1px solid $clr-color;
    transition: all 0.3s ease-in-out;
    transform: scaleX(1);
    transform-origin: bottom left;
    -webkit-background-clip: text;
  }

  .profile-nav li a.active {
    border-bottom: 2px solid $clr-color;
  }
}


.little-profile-activity .pro-img-activity img {
  width: 128px;
  height: 128px;
  border: 1px solid #c1c1c1;
  align-items: center;
  align-content: center;
}

.pro-img-activity {
  margin-top: -150px;

}

#upload {
  cursor: pointer;
}

#upload-label {
  position: absolute;
  top: 50%;
  left: 1rem;
  transform: translateY(-50%);
}

.image-area {
  border: 3px dashed rgba(0, 0, 0, 0.7);
  padding: 1rem;
  position: relative;
}

.image-area::before {
  content: "\f03e";
  color: #000000;
  font-weight: bold;
  font-family: FontAwesome;
  text-transform: uppercase;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  font-size: 0.9rem;
  z-index: 1;
}

.image-area img {
  z-index: 2;
  position: relative;
}

#formAddress .modal-dialog .card {
  .multi_step_form {
    display: block;
    overflow-x: hidden;
  }

  .multi_step_form #msform {
    min-height: 400px;
    height: auto;
    max-width: 820px;
    margin: 0 auto;
    z-index: 1;
  }

  .multi_step_form #msform fieldset {
    border: 0;
    position: relative;
    width: 100%;
    left: 0;
    right: 0;

    .form-group .form-custom{
      background: none;
      border: 1px solid black;
    }
  }


  .multi_step_form #msform fieldset:not(:first-of-type) {
    display: none;
  }

  .multi_step_form #msform fieldset h3 {
    font: 500 18px/35px "Roboto", sans-serif;
    color: #3f4553;
  }

  .multi_step_form #msform fieldset h6 {
    font: 400 15px/28px "Roboto", sans-serif;
    color: #5f6771;
    padding-bottom: 30px;
  }

  .multi_step_form #msform fieldset .product_select .list {
    width: 100%;
  }


  .multi_step_form #msform #progressbar {
    margin-bottom: 30px;
    overflow: hidden;
  }

  .multi_step_form #msform #progressbar li {
    list-style-type: none;
    color: #99a2a8;
    font-size: 9px;
    width: calc(100% / 3);
    float: left;
    position: relative;
    font: 500 13px/1 "Roboto", sans-serif;
  }

  .multi_step_form #msform #progressbar li:nth-child(2):before {
    content: "\f21d ";
    font-family: FontAwesome;
  }

  .multi_step_form #msform #progressbar li:nth-child(3):before {
    content: "\f00c";
    font-family: FontAwesome;
  }

  .multi_step_form #msform #progressbar li:before {
    content: "\f2bb";
    font-family: FontAwesome;
    width: 50px;
    height: 50px;
    line-height: 50px;
    display: block;
    background: #eaf0f4;
    border-radius: 50%;
    margin: 0 auto 10px auto;
  }

  .multi_step_form #msform #progressbar li:after {
    content: '';
    width: 100%;
    height: 10px;
    background: #eaf0f4;
    position: absolute;
    left: -50%;
    top: 21px;
    z-index: -1;
  }

  .multi_step_form #msform #progressbar li:last-child:after {
    width: 150%;
  }

  .multi_step_form #msform #progressbar li.active {
    color: #007bff;
  }

  .multi_step_form #msform #progressbar li.active:before, .multi_step_form #msform #progressbar li.active:after {
    background: #007bff;
    color: white;
  }

  .multi_step_form #msform .action-button {
    background: #2d8ffa;
    color: white;
    border: 0 none;
    border-radius: 5px;
    cursor: pointer;
    min-width: 130px;
    font: 700 14px/40px "Roboto", sans-serif;
    border: 1px solid #007bff;
    margin: 0 5px;
    text-transform: uppercase;
    display: inline-block;
  }

  .multi_step_form #msform .action-button:hover, .multi_step_form #msform .action-button:focus {
    background: #405867;
    border-color: #405867;
  }

  .multi_step_form #msform .previous_button {
    background: transparent;
    color: #99a2a8;
    border-color: #99a2a8;
  }

  .multi_step_form #msform .previous_button:hover, .multi_step_form #msform .previous_button:focus {
    background: #405867;
    border-color: #405867;
    color: #fff;
  }
}
